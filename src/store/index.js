import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '@/router/index.js'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		items: [],
		item: null
	},
	mutations: {
		SET_ITEMS_TO_STATE: (state, items) => {
			state.items = items
		},
		SET_ITEM_TO_STATE: (state, item) => {
			state.item = item
		},
		ADD_ITEM: state => {
			state.items
		},
		UPDATE_ITEM: (state, updatedItem) => {
			const index = state.items.findIndex(item => item.id === updatedItem.id)
			if (index !== -1) {
				state.items.splice(index, 1, updatedItem)
			}
		},
		DELETE_ITEM: (state, itemId) => {
			state.items = state.items.filter(item => itemId !== item.id)
		}
	},
	actions: {
		GET_ITEMS_FROM_API({ commit }) {
			return axios
				.get('http://localhost:8000/items')
				.then(items => {
					commit('SET_ITEMS_TO_STATE', items.data)
					return items
				})
				.catch(error => {
					console.log(error)
					return error
				})
		},
		GET_ITEM_FROM_API({ commit }, itemId) {
			return axios
				.get('http://localhost:8000/items/' + itemId)
				.then(item => {
					commit('SET_ITEM_TO_STATE', item.data)
					return item.data
				})
				.catch(error => {
					console.log(error)
					return error
				})
		},
		ADD_ITEM_TO_API({ commit }, itemData) {
			return axios
				.post('http://localhost:8000/items', itemData)
				.then(response => {
					commit('ADD_ITEM', response.data)
					router.push({ name: 'Catalog' })
				})
				.catch(error => {
					console.log(error)
					return error
				})
		},
		UPDATE_ITEM_IN_STORE({ commit }, item) {
			return axios
				.put('http://localhost:8000/items/' + item.id, item)
				.then(response => {
					commit('UPDATE_ITEM', response.data)
					return response
				})
				.catch(error => {
					console.log(error)
					return error
				})
		},
		DELETE_ITEM_FROM_STORE({ commit }, itemId) {
			return axios
				.delete('http://localhost:8000/items/' + itemId)
				.then(() => {
					commit('DELETE_ITEM', itemId)
				})
				.catch(error => {
					console.log(error)
					return error
				})
		}
	},
	getters: {
		ITEMS(state) {
			return state.items
		},
		ITEM(state) {
			return state.item
		}
	}
})
