import Vue from 'vue'
import VueRouter from 'vue-router'
import Catalog from '../views/Catalog'
import NewItem from '../views/NewItem.vue'
import ItemInfo from '../components/ShopItems/ItemInfo.vue'

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'Catalog',
		component: Catalog
	},
	{
		path: '/item/:id',
		name: 'Item info',
		component: ItemInfo
	},
	{
		path: '/items-add',
		name: 'Add Item',
		component: NewItem
	}
]

const router = new VueRouter({
	routes
})

export default router
